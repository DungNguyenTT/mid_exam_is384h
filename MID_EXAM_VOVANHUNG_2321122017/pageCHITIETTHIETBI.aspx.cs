﻿using MID_EXAM_VOVANHUNG_2321122017.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MID_EXAM_VOVANHUNG_2321122017
{
    public partial class pageCHITIETTHIETBI : System.Web.UI.Page
    {
        public void Soluong(int Soluong)
        {
            for (int i = 1; i <= Soluong; i++)
                this.drlSOLUONG.Items.Add(i.ToString());
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            String path = Server.MapPath("App_Data\\dbTHIETBIMAYTINHCU.mdf");
            XuLyDuLieu xuLyDuLieu = new XuLyDuLieu(path);

            String MATHIETBI = Request.QueryString.Get("MATHIETBI");
            if (MATHIETBI != null)
            {
                string SQL = "SELECT * FROM THIETBI WHERE MATHIETBI=" + "'"+ MATHIETBI +"'";
                DataTable tb = xuLyDuLieu.layBang(SQL);
                this.Repeater2.DataSource = tb;
                this.Repeater2.DataBind();
                Soluong(Convert.ToUInt16(tb.Rows[0]["SOLUONG"]));
            }
        }
    }
}
﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MID_EXAM_VOVANHUNG_2321122017.App_Code
{
    public class XuLyDuLieu
    {
        SqlConnection con;
        public XuLyDuLieu(String path)
        {
            con = new SqlConnection();
            con.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + path + ";Integrated Security=True";


        }
        private void MoketNoi()
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
        }
        private void DongketNoi()
        {
            if (con.State == ConnectionState.Open)
                con.Close();
        }
        public DataTable layBang(String SQL)
        {
            this.MoketNoi();
            DataTable tb = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(SQL, con);
            adp.Fill(tb);
            this.DongketNoi();
            return tb;
        }
        public int thucThiSQL(String SQL)
        {
            this.MoketNoi();
            SqlCommand cmd = new SqlCommand(SQL, con);
            int k = cmd.ExecuteNonQuery();
            this.DongketNoi();
            return k;
        }
        public void layDanhSachBang(ref DataSet ds, String SQL)
        {
            ds.Tables.Add(this.layBang(SQL));
        }
        public object layGiaTriDon(String SQL)
        {
            this.MoketNoi();
            SqlCommand cmd = new SqlCommand(SQL, con);
            object value = cmd.ExecuteScalar();
            this.DongketNoi();
            return value;
        }
        public DataTable layBangThuTuc(String tenthutuc, SqlParameter[] pr)
        {
            this.MoketNoi();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = tenthutuc;
            if (pr != null)
                cmd.Parameters.AddRange(pr);
            DataTable tb = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(tb);
            this.DongketNoi();
            return tb;
        }
        public int thucThiThuTuc(String tenthutuc, SqlParameter[] pr)
        {
            this.MoketNoi();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = tenthutuc;
            if (pr != null)
                cmd.Parameters.AddRange(pr);
            int k = cmd.ExecuteNonQuery();
            this.DongketNoi();
            return k;
        }
    }
}